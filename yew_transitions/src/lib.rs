
pub use transition::CssTransition as CssTransition;
pub use transition::Data          as Props;

pub mod transition {
    use yew::{function_component, Html, html, Properties, Callback, Component, Children};
    

    #[derive(Clone, PartialEq, Properties)]
    pub struct Data {
        pub children: Children,
    }


    pub enum Selectors {
        Opacity
    }


    #[function_component(CssTransition)]
    pub fn css(props: &Data) -> Html {
        
        let call = Callback::from(
            |x: i32| ()
        );

        html! {
            <span class={"css_transition_mapping"}>
                {props.children.clone()}            
            </span>
        }

    }
}

