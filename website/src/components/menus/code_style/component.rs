use yew::{
    Html,
    html,
    function_component,
};

use super::{Props, get_random_u8};

use super::jsx_layout::JSXCodeLayout;
use super::php_layout::PHPCodeLayout;
use super::c_layout::CLangCodeLayout;
use super::rust_layout::RustCodeLayout;

#[function_component(MenuCodeStylized)]
pub fn component(props: &Props) -> Html {

        let data = props.data.clone();

        let go = match get_random_u8(0, 3) {
            0 => html!{<JSXCodeLayout   data={data}/>},
            1 => html!{<PHPCodeLayout   data={data}/>},
            2 => html!{<CLangCodeLayout data={data}/>},
            _ => html!{<RustCodeLayout  data={data}/>}
        };


    html! {
        <>
        {go}
        </>
    }
}