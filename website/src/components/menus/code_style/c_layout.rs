use yew::{
    Html,
    html, 
    classes,
    function_component
};

use super::{
    Props,
    add_line,
    count_space,
    add_line_array,
    prepare_link_menu,
    get_greatest_word
};

#[inline]
fn prepare_link_menu_content(name: &String, span_class: &String, space_string_len: usize) -> Html {
    html! {
    <>
        <span class={span_class}>
            {"        "}{name.to_lowercase()}{"();"}{count_space(name.len(), space_string_len)}
        </span>
    </>
    }
}

#[inline]
fn prepare_menu(props: &Props) -> Html {
    let space_string_len: usize = get_greatest_word(&props.data);
    let mut first_if_stmt: bool = true;

    html! {
        {
        props.data.iter().map(|el| {
            let link_classes = classes!(&el.link_class_name, "mainCodeMenu");
            let content      = prepare_link_menu_content(
                &el.name,
                &el.span_class_name,
                space_string_len
            );

            // Prepare if:else if statements
            let else_stmt = {
                if first_if_stmt { 
                    first_if_stmt = false;
                    "    ".to_string()
                } else {
                    "    } else ".to_string()
                }
            };

            html!{
            <>
            <div>
                <span>{else_stmt}{"if(_) {"}</span>
            </div>
            <div> 
                {prepare_link_menu(el.route.clone(), link_classes, content)}
            </div>
            </>
        }}).collect::<Html>()
        }
    }
}


/**
 *  The menu was formatted in HTML looks like the example below:
 *  
 *  void
 *  main(_)
 *      char *p = getenv("QUERY_STRING"); {
 *      if(_) {
 *          path_01();
 *      } else if(_) {
 *          path_02();
 *      }
 *      ...
 *      else if(_) {
 *          path_N();
 *      }
 *  }
 */
#[function_component(CLangCodeLayout)]
pub fn clang_layout(props: &Props) -> Html {
    html!{
        <div class={"specialCodeArea clang"}>
            {add_line_array(vec![
                "void",
                "main(_) {",
                "    char *p = getenv(\"QUERY_STRING\"); {"
            ])}
            {prepare_menu(props)}
            {add_line("    }")}
            {add_line("}")}
        </div>
    }
}