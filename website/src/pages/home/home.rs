use yew::{Html, html, function_component};

use crate::{
    components::buttons::{
        home::Props,
        home::HomeButton,
    },
    components::menus::{
        code_style::MenuCodeStylized,
        code_style::SingleProps
    },

    pages::ArtworkCodeLayout::{
        CodeRoot, 
        ArtworkRoot
    },

    pages::CodeRoute::{
        Web,
        Games,
    }
};

#[function_component(Home)]
pub fn get() -> Html {

    let lhs = Props {
        class_name: String::from("codes"),
        name      : String::from("Code"),
        route     : CodeRoot
    };

    let rhs = Props {
        class_name: String::from("artworks"),
        name      : String::from("Artwork"),
        route     : ArtworkRoot
    };


    let data = vec![ SingleProps {
        link_class_name: String::from("web"),
        span_class_name: String::from("linkCode web"),
        name: String::from("web"),
        route: Web
    },
    SingleProps {
        link_class_name: String::from("games"),
        span_class_name: String::from("linkCode games"),
        name: String::from("games"),
        route: Games
    },
    SingleProps {
        link_class_name: String::from("layout"),
        span_class_name: String::from("linkCode layout"),
        name: String::from("layout"),
        route: Games
    }];


    html! {
        <>
        <MenuCodeStylized data={data}/>
        <nav class={String::from("homeMenu")} >
            // Code Side
            <HomeButton
                class_name={lhs.class_name}
                name={lhs.name}
                route={lhs.route} 
            />
            // Artwork Site
            <HomeButton
                class_name={rhs.class_name}
                name={rhs.name}
                route={rhs.route} 
            />
        </nav>
        </>
    }
}
