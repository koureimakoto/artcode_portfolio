use yew::{Html, html, function_component};

#[function_component(Code)]
pub fn get() -> Html {
    html! {
        <h1 class={"trans"}>{"Code"}</h1>
    }
}    
